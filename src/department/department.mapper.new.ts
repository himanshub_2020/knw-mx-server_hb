import { createMapper } from '@automapper/core';
import { classes } from '@automapper/classes';
import { AutoMap } from '@automapper/classes';

export const mapper = createMapper({
    name: 'department',
    pluginInitializer: classes,
});

export class DepartmentBase {
    @AutoMap()
    name: string;

    @AutoMap()
    description: string;
}

export class DepartmentRequest extends DepartmentBase {
    // @AutoMap()
    // name: string;

    // @AutoMap()
    // description: string;
}

export class DepartmentRequestDto extends DepartmentBase {
    // @AutoMap()
    // name: string;

    // @AutoMap()
    // description: string;
}

export class DepartmentDb extends DepartmentBase {
    @AutoMap()
    addedBy: string;

    @AutoMap()
    modifiedBy: string;

    @AutoMap()
    status: string;
}

export class DepartmentDbDto extends DepartmentBase {
    @AutoMap()
    addedBy: string;

    @AutoMap()
    modifiedBy: string;

    @AutoMap()
    status: string;
}

export class DepartmentRes extends DepartmentBase {

    @AutoMap()
    addedBy: string;

    @AutoMap()
    modifiedBy: string;

    @AutoMap()
    status: string;

    @AutoMap()
    createdAt: string;

    @AutoMap()
    updatedAt: string;
}

export class DepartmentResDto {

    @AutoMap()
    addedBy: string;

    @AutoMap()
    modifiedBy: string;

    @AutoMap()
    status: string;

    @AutoMap()
    createdAt: string;

    @AutoMap()
    updatedAt: string;

    @AutoMap()
    name: string;
}

mapper.createMap(DepartmentRequest, DepartmentRequestDto);
mapper.createMap(DepartmentDb, DepartmentDbDto);
mapper.createMap(DepartmentRes, DepartmentResDto);

export function getDepartmentRequestDto(source: object | Array<any>): DepartmentRequestDto {
    return mapper.map(source, DepartmentRequestDto, DepartmentRequest);
}

export function getDepartmentDbDto(source: object | Array<any>): DepartmentDbDto {
    return mapper.map(source, DepartmentDbDto, DepartmentDb);
}

export function getDepartmentResDto(source: object | Array<any>): DepartmentResDto {
    return mapper.map(source, DepartmentResDto, DepartmentRes);
}


