import { Module, NestModule, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { departmentController } from "./department.controller";
import { DepartmentService } from "./department.service";
// import { SharedModule } from "../shared/shared.module";
import { DepartmentSchema } from './department.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthMiddleware } from "../shared/middlewares/auth.middleware";
@Module({
    imports: [
        MongooseModule.forFeature([{ name: 'Department', schema: DepartmentSchema }], 'KNOW-MX_DB')
    ],
    controllers: [departmentController],
    providers: [DepartmentService],
    exports: []
})
export class DepartmentModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(AuthMiddleware)
            // .exclude(
            //     { path: 'cats', method: RequestMethod.GET },
            //     { path: 'cats', method: RequestMethod.POST },
            //     'cats/(.*)',
            // )
            .exclude('api/v1/department/deleteDepartment')
            .forRoutes(departmentController);
    }
}
