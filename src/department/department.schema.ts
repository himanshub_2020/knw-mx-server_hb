// import * as mongoose from 'mongoose';
// import * as bcrypt from 'bcrypt';
import { ApiProperty, ApiCreatedResponse, ApiBody, ApiQuery, ApiHeader } from '@nestjs/swagger';

// export const DepartmentSchema = new mongoose.Schema(
//     {
//         addedBy: {
//             type: String,
//             // ref: 'User',  User schema will be created
//             default: 'admin (name/id of admin from admin table)'
//         },
//         description: {
//             type: String,
//             required: true,
//         },
//         // addedBy
//         modifiedBy: {
//             type: String,
//             // ref: 'User',  User schema will be created
//             default: 'admin (name/id of admin from admin table)'
//         },
//         name: {
//             type: String,
//             required: true,
//             unique: true
//         },
//         //Using enum here - because our values are pre-defined/fixed
//         status: {
//             type: String,
//             enum: ['active', 'delete', 'draft'], default: 'draft'
//         },

//     },
//     { timestamps: true, versionKey: false },
// );
export const departmentModelName = "department";



import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';


export type ImageDocument = Images & Document;
export interface imageInterface {
    path: string;
    description: string;
}

@Schema()
export class Images {
    @ApiProperty({ type: String, description: 'Name of department1updated', required: true })
    @Prop()
    path: string;

    @Prop()
    description: string;
}
export const ImagesSchema = SchemaFactory.createForClass(Images);

//--------------------------------------------
export type StatusesDocument = Statuses & Document;
@Schema()
export class Statuses {
    @Prop()
    online: string;

    @Prop()
    verified: string;
}
export const StatusesSubSchema = SchemaFactory.createForClass(Statuses);
//------------------------------------------------------

export type CatDocument = Department & Document;

@Schema()
export class Department {
    @ApiProperty({ type: String, description: 'Name of department1updated', required: true })
    @Prop()
    name: string;

    @Prop({ default: "Active" })
    status: string;

    @ApiProperty({ type: String, description: 'Description', required: true })
    @Prop([{ type: mongoose.Schema.Types.ObjectId }])
    department: string[];

    @ApiProperty({ type: [Images], description: 'Name of department1updated', required: true })
    @Prop({ type: [ImagesSchema] })
    images: string[]


    @Prop({ type: { StatusesSubSchema } })
    statuses: string

}

export const DepartmentSchema = SchemaFactory.createForClass(Department);