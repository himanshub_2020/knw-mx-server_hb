
export class DepartmentRequestModel {
    constructor() {
        this['name'] = "",
            this['description'] = ""
    }
}

export class DepartmentDbModel {
    constructor() {
        this['name'] = "",
            this['description'] = "",
            this['addedBy'] = "admin",
            this['modifiedBy'] = "admin",
            this['status'] = "draft"
    }
}

export class DepartmentResponseModel {
    constructor() {
        this['name'] = "",
            this['description'] = "",
            this['addedBy'],
            this['modifiedBy'] = "",
            this['status'] = "",
            this['createdAt'] = "",
            this['updatedAt'] = ""
    }
}