import { Controller, Get, Res, Req, Body, Query, Post, Delete, Put, UseGuards } from '@nestjs/common';
import { DepartmentService } from "./department.service";
import { ResponseService } from "../shared/response.service";
import { ApiProperty, ApiCreatedResponse, ApiBody, ApiQuery, ApiHeader } from '@nestjs/swagger';
import { AddDepartmentDTO, EditDepartmentDTO, fetchDepartmentsDTO, AddDepartmentDTO1, fetchDepartmentModel, addDepartmentModel, idModel, headerModel } from "./department.model";
import { ErrLogsService } from "../shared/err.logs.service";
import { ValidatorService } from "../shared/validator.service";
const controller = "src/department/departmentController";
import { AuthGuard } from "../shared/guards/auth.guard";
import { Department} from  "./department.schema";

import { PictureGuide} from "../picture-guide/picture-guide-schema";


@Controller('department')  //endpoints - localhost:4000/api/v1/department
@UseGuards(AuthGuard)
export class departmentController {
    constructor(private readonly departmentService: DepartmentService,
        private responseService: ResponseService,
        private errLogsService: ErrLogsService,
        private validatorService: ValidatorService,
    ) { }


    @ApiCreatedResponse({ description: 'Add a new department' })
    @ApiBody({ type: PictureGuide })
    @ApiHeader(headerModel)
    @Post('addDepartment') //endpoints - localhost:4000/api/v1/department/addDepartment

    async addDepartment(@Req() request, @Body() bodyData, @Res() response): Promise<void> {
        try {
            this.validatorService.validateBody(bodyData, addDepartmentModel);
            const result = await this.departmentService.addDepartment(request, bodyData);
            this.responseService.createdRes(response, result, `Record added successfully!`, [result].length);
        }
        catch (err) {
            //Common ERR SERVICE that can used by any controller
            this.errLogsService.storeLog(err, `${controller}/addDepartment`);
            this.responseService.errRes(response, err);
        }
    }

    @ApiHeader(headerModel)
    @ApiQuery({ name: 'id', description: 'Id of department which you want to delete', required: true })
    @Delete('deleteDepartment')
    async deleteDepartment(@Req() request, @Query() query, @Res() response): Promise<any> {
        try {
            this.validatorService.validateParams(query, idModel);
            const result = await this.departmentService.deleteDepartment(request, query);
            return this.responseService.noContentRes(response);
        }
        catch (err) {
            //Common ERR SERVICE that can used by any controller
            this.errLogsService.storeLog(err, `${controller}/deleteDepartment`);
            return this.responseService.errRes(response, err);
        }
    }

    @ApiHeader(headerModel)
    @ApiBody({ type: EditDepartmentDTO })
    @ApiQuery({ name: 'id', description: 'Id of department which you want to edit', required: true })
    @Put('editDepartment')
    async editDepartment(@Req() request, @Body() body, @Query() query, @Res() response): Promise<any> {
        try {

            this.validatorService.validateParams(query, idModel);
            this.validatorService.validateBody(body, addDepartmentModel);
            const result = await this.departmentService.editDepartment(request, body, query);
            return this.responseService.noContentRes(response);
        }
        catch (err) {
            //Common ERR SERVICE that can used by any controller
            this.errLogsService.storeLog(err, `${controller}/editDepartment`);
            return this.responseService.errRes(response, err);
        }
    }

    @ApiHeader(headerModel)
    @ApiBody({ type: fetchDepartmentsDTO })
    @Post('fetchDepartment')
    async fetchDepartment(@Req() request, @Body() body, @Res() response, @Query() query): Promise<any> {
        try {
            this.validatorService.validateBody(body, fetchDepartmentModel);
            const result = await this.departmentService.fetchDepartment(request, body, query);
            return this.responseService.successRes(response, [result], `Fethced successfully!`, result.length);
        }
        catch (err) {
            //Common ERR SERVICE that can used by any controller
            this.errLogsService.storeLog(err, `${controller}/fetchDepartment`);
            return this.responseService.errRes(response, err);
        }
    }
}
