import { Injectable, Body } from '@nestjs/common';
import { modelNames } from 'mongoose';
import { CommonQueryService } from "../shared/commonQuery.services"
import { getModel } from "../shared/dynamicDb";
import { DepartmentSchema, departmentModelName } from "./department.schema";
import { addDepartmentModel } from './department.model';
import { Connection } from 'mongoose';
console.log(">>>>>>>>>>", process.env.port);

import { InjectConnection } from '@nestjs/mongoose';
import { createElasticData, getElasticData } from "../shared/functions/elastic.search";
var mapper = require('automapper-js');
import { DepartmentRequestModel, DepartmentDbModel, DepartmentResponseModel } from "./department.mapper";
import { getDepartmentRequestDto, getDepartmentDbDto, getDepartmentResDto } from "./department.mapper.new";
@Injectable()
export class DepartmentService {
    constructor(private commonQueryService: CommonQueryService,
        @InjectConnection('KNOW-MX_DB') private defaultConnection: Connection,
    ) { }
    async addDepartment(request: object, body: object): Promise<object> {
        console.log(">>>>>>>>>>", body);

        const departmentModel = await getModel(request, DepartmentSchema, departmentModelName, this.defaultConnection);

        /*Old mapping code
        //----------------------------auto-mapping
        body = mapper(DepartmentRequestModel, body);
        const dataToSave = mapper(DepartmentDbModel, body);
        //----------------------------
        let condition = { name: dataToSave['name'] },
            elasticType = "deparments";
        if (await this.checkIfAlreadyExists(departmentModel, condition)) {
            throw `Record already exists!`;
        } else {
            const result = await this.commonQueryService.addNewRecord(departmentModel, dataToSave);
            //---------auto-mapping
            let responseData = mapper(DepartmentResponseModel, result);
            console.log("<<<<<<<<<<<<<<<<<", getUserDto());
            //---------

            // await createElasticData(request, elasticType, body);
            // let conditon1 = { name: body['name'] };
            // await getElasticData(request, conditon1);
            return responseData;
        }*/


        // function create(o: object , b: string): void {

        // }
        // create(undefined , "");



        //----------------------------auto-mapping
        // body = getDepartmentRequestDto(body);
        // const dataToSave = getDepartmentDbDto(body);
        //----------------------------
        let condition = { name: body['name'] },
            elasticType = "deparments";
        if (await this.checkIfAlreadyExists(departmentModel, condition)) {
            throw `Record already exists!`;
        } else {
            console.log("body########", body);
            const result = await this.commonQueryService.addNewRecord(departmentModel, body);
            //---------auto-mapping


            let tempObj = {
                "_id": "60125b012edd8b6d38fed86f",
                "addedBy": "admin (name/id of admin from admin table)",
                "modifiedBy": null,
                "status": undefined,
                "description": "this is for testing purpose",
                "createdAt": "2021-01-28T06:34:41.214Z",
                "updatedAt": "2021-02-01T11:15:31.610Z",
                "__v": 0
            };
            let responseData = getDepartmentResDto(result);
            // console.log("<<<<<<<<<<<<<<<<<", responseData);
            //---------

            // await createElasticData(request, elasticType, body);
            // let conditon1 = { name: body['name'] };
            // await getElasticData(request, conditon1);
            return result;
        }
    }

    async checkIfAlreadyExists(departmentModel: any, condition: any): Promise<boolean> {
        return await this.commonQueryService.checkIfAlreadyExists(departmentModel, condition);
    }

    async editDepartment(request: any, body: any, query: any): Promise<any> {
        const departmentModel = await getModel(request, DepartmentSchema, departmentModelName, this.defaultConnection);
        const { id } = query,
            { name, description } = body,
            condition = { _id: id },
            dataToUpdate = { name, description };
        return await this.commonQueryService.editSingleRecord(departmentModel, condition, dataToUpdate);
    }

    async deleteDepartment(request: any, query: any): Promise<any> {
        const { id } = query,
            condition = { _id: id },
            dataToUpdate = { status: 'delete' };
        const departmentModel = await getModel(request, DepartmentSchema, departmentModelName, this.defaultConnection);
        return await this.commonQueryService.editSingleRecord(departmentModel, condition, dataToUpdate);
    }

    async fetchDepartment(request: any, body: any, query: any): Promise<any> {
        const { isSingle, pageNumber, limit, id } = body;
        const condition = {},
            projection = {},
            skip = (parseInt(pageNumber) - 1) * parseInt(limit),
            sort = "";
        let result = "";
        const departmentModel = await getModel(request, DepartmentSchema, departmentModelName, this.defaultConnection);

        if (isSingle) {
            if (!id) {
                throw "Department id is not found!"
            }
            const condition = { _id: id };
            return await this.commonQueryService.fetchSingleCollectionRecord(departmentModel, condition, projection, skip, limit, sort);
        } else {
            return await this.commonQueryService.fetchSingleCollectionRecord(departmentModel, condition, projection, skip, limit, sort);
        }
    }
}
