import { ApiProperty } from '@nestjs/swagger';
export class Sub {
    @ApiProperty({ type: String, description: 'Name of department', required: true })
    name: String;
    @ApiProperty({ type: String, description: 'Description', required: true })
    test: String;
}

export class AddDepartmentDTO {
    @ApiProperty({ type: String, description: 'Name of department', required: true })
    name: String;
    @ApiProperty({ type: Sub, description: 'Description', required: true })
    description: String;
}








export class AddDepartmentDTO1 {
    // @ApiProperty({ type: String, description: 'Name of department', required: true })
    name: String;
    // @ApiProperty({ type: String, description: 'Description', required: true })
    description: Number;
}
export class EditDepartmentDTO {
    @ApiProperty({ type: String, description: 'Name of department', required: true })
    name: String;
    @ApiProperty({ type: String, description: 'Description', required: true })
    description: String;
}

export class fetchDepartmentsDTO {
    @ApiProperty({ type: Boolean, description: 'indicates you want to fetch single/multiple records', required: true })
    isSingle: Boolean;
    @ApiProperty({ type: String, description: 'if you want to fetch single record then pass its id here', required: false })
    id: String;
    @ApiProperty({ type: Number, required: true })
    pageNumber: Number;
    @ApiProperty({ type: Number, required: true })
    limit: Number;
}
export let addDepartmentModel = {
    name: 'required|min:3|max:30',
    description: 'required|min:10|max:30',
    department: 'required|array',
    images: 'required|array',
}

export let idModel = {
    id: 'required',
}
export const fetchDepartmentModel = {
    isSingle: 'required|boolean',
    id: 'string',
    pageNumber: 'required|numeric|min:1',
    limit: 'required|numeric|min:1'
}

export const headerModel = {
    name: 'hostName',
    description: 'Host name for database switching',
    required: true
}