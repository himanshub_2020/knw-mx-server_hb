import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
const cron = require("./cron");

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('api/v1');
  const options = new DocumentBuilder()
    .setTitle('KNOW-MX')
    .setDescription('The KNOW-MX API description')
    .setVersion('2.0')
    .addTag('KNOW-MX')
    .build();
  const document = SwaggerModule.createDocument(app, options, {
    operationIdFactory: (
      controllerKey: string,
      methodKey: string
    ) => methodKey
  });
  SwaggerModule.setup('api', app, document);
  await app.listen(process.env.SERVER_PORT, () => {
    console.info("****Server is listening port", process.env.SERVER_PORT, "*****");
  });

}
bootstrap();
