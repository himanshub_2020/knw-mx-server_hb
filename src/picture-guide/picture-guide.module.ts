import { Module, Controller } from '@nestjs/common';
import { PictureGuideController } from "./picture-guide-controller";
import { PictureGuideService } from "./picture-guide-service";
@Module({
    controllers: [PictureGuideController],
    providers: [PictureGuideService]
})
export class PictureGuideModule { }
