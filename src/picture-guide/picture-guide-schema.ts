
export const PictureGuideModelName = "pictureGuide";

import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';

import { ApiProperty } from '@nestjs/swagger';
//steps subschema------------------
export type StepsDocument = Steps & Document;
@Schema()
export class Steps {

    @ApiProperty({ type: String, description: 'Set step text', required: true })
    @Prop({ type: String, required: true })
    StepText: string;

    @ApiProperty({ type: String, description: 'Set image url', required: true })
    @Prop({ type: String, required: true })
    ImageUrl: string;

    @ApiProperty({ type: String, description: 'Set image name', required: true })
    @Prop({ type: String, required: true })
    ImageName: string;

    @ApiProperty({ type: Number, description: 'Set sequence number', required: true })
    @Prop({ type: Number, required: true })
    Sequence: number;

    @ApiProperty({ type: Array, description: 'Set tips', required: true })
    @Prop([{ type: String, required: true }])
    Tips: string;

    @ApiProperty({ type: Array, description: 'Set warnings', required: true })
    @Prop([{ type: String, required: true }])
    Warnings: string;

    @ApiProperty({ type: Number, description: 'Set layout number', required: true })
    @Prop({ type: Number, required: true })
    Layout: number;
}
export const StepsSubSchema = SchemaFactory.createForClass(Steps);
//-------------------------

//-----------language subschema--------------
export type LanguageDocument = Language & Document;
@Schema()
export class Language {

    @ApiProperty({ type: String, description: 'Set name', required: true })
    @Prop({ type: String, required: true })
    Name: string;

    @ApiProperty({ type: String, description: 'Set direction', required: true })
    @Prop({ type: String, required: true })
    Direction: string;

    @ApiProperty({ type: String, description: 'Set bcp47code', required: true })
    @Prop({ type: String, required: true })
    Bcp47code: string;

    @ApiProperty({ type: String, description: 'Set code', required: true })
    @Prop({ type: String, required: true })
    Code: string;
}
export const LanguageSubSchema = SchemaFactory.createForClass(Language);
//-------------------

//-------version subschema---------
export type VersionDocument = Version & Document;
@Schema()
export class Version {

    @ApiProperty({ type: Number, description: 'Set number', required: true })
    @Prop({ type: Number, required: true })
    Number: number;

    @ApiProperty({ type: String, description: 'Set name', required: true })
    @Prop({ type: String, required: true })
    Name: string;

    @ApiProperty({ type: String, description: 'Set parent', required: true })
    @Prop({ type: String, required: true })
    Parent: string;

    @ApiProperty({ type: String, description: 'Set base', required: true })
    @Prop({ type: String, required: true })
    Base: string;
}
export const VersionSubSchema = SchemaFactory.createForClass(Version);
//----------------

//-------link module subschema---------
export type LinkAnotherModuleDocument = LinkAnotherModule & Document;
@Schema()
export class LinkAnotherModule {

    @ApiProperty({ type: String, description: 'Set content _id', required: true })
    @Prop({ type: mongoose.Schema.Types.ObjectId, required: true })
    ContentId: string;

    @ApiProperty({ type: String, description: 'Set content type', required: true })
    @Prop({ type: String, required: true })
    ContentType: string;

    @ApiProperty({ type: String, description: 'Set service code', required: true })
    @Prop({ type: String, required: true })
    ServiceCode: string;

}
export const LinkAnotherModuleSubSchema = SchemaFactory.createForClass(LinkAnotherModule);
//----------------

//-------Urls subschema---------
export type UrlsDocument = Urls & Document;
@Schema()
export class Urls {

    @ApiProperty({ type: String, description: 'Set display name', required: true })
    @Prop({ type: String, required: true })
    DisplayName: string;

    @ApiProperty({ type: String, description: 'Set display link', required: true })
    @Prop({ type: String, required: true })
    Link: string;
}
export const UrlsSubSchema = SchemaFactory.createForClass(Urls);
//----------------

//Root schema starts---------
export type PictureGuideDocument = PictureGuide & Document;
@Schema()
export class PictureGuide {
    @ApiProperty({ type: String, description: 'Set title here', required: true })
    @Prop({ required: true, type: String })
    Title: string;

    @ApiProperty({ type: Array, description: 'Set keywards', required: true })
    @Prop([{ type: String, required: true }])
    Keywords: string[];

    @ApiProperty({ type: [Steps], description: 'Steps entry here', required: true })
    @Prop({ type: [StepsSubSchema] })
    Steps: string[]

    @ApiProperty({ type: String, description: 'Set embed url', required: true })
    @Prop({ required: true, type: String })
    EmbedUrl: string;

    @ApiProperty({ type: String, description: 'Set video url', required: true })
    @Prop({ required: true, type: String })
    VideoUrl: string;

    @ApiProperty({ type: String, description: 'Set category path', required: true })
    @Prop({ required: true, type: String })
    CategoryPath: string;

    @ApiProperty({ type: Date, description: 'Set start date', required: true })
    @Prop({ required: true, type: Date })
    StartDate: string;

    @ApiProperty({ type: Date, description: 'Set end date', required: true })
    @Prop({ required: true, type: Date })
    EndDate: string;

    @ApiProperty({ type: Language, description: 'Set languages properties', required: true })
    @Prop({ type: LanguageSubSchema })
    Language: string

    @ApiProperty({ type: String, description: 'Set language group id', required: true })
    @Prop({ required: true, type: String })
    LanguageGroupId: string;

    @ApiProperty({ type: Version, description: 'Set version properties', required: true })
    @Prop({ type: VersionSubSchema })
    Version: string

    @ApiProperty({ type: [LinkAnotherModule], description: 'Steps module items', required: true })
    @Prop({ type: [LinkAnotherModuleSubSchema] })
    LinkAnotherModules: string[]

    @ApiProperty({ type: Array, description: 'Set channels _ids', required: true })
    @Prop([{ type: mongoose.Schema.Types.ObjectId, required: true }])
    Channels: string;

    @ApiProperty({ type: Array, description: 'Set departments _ids', required: true })
    @Prop([{ type: mongoose.Schema.Types.ObjectId, required: true }])
    Departments: string;

    @ApiProperty({ type: String, description: 'Set category _id', required: true })
    @Prop({ type: mongoose.Schema.Types.ObjectId, required: true })
    CategoryId: string;

    @ApiProperty({ type: Array, description: 'Set media _ids', required: true })
    @Prop([{ type: mongoose.Schema.Types.ObjectId, required: true }])
    Media: string;

    @ApiProperty({ type: [Urls], description: 'Steps url items', required: true })
    @Prop({ type: [UrlsSubSchema] })
    Urls: string[]
}

export const PictureGuideSchema = SchemaFactory.createForClass(PictureGuide);