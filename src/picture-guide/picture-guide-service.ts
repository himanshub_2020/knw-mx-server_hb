import { Injectable, Body } from '@nestjs/common';
import { Request } from 'express';
import { CommonQueryService } from "../shared/commonQuery.services"
import { getModel } from "../shared/dynamicDb";
import { Connection } from 'mongoose';
import { InjectConnection } from '@nestjs/mongoose';
import { FileService } from "../shared/file-service";
import { PictureGuideModelName, PictureGuideSchema } from "./picture-guide-schema";
// /-------------------
const formidable = require('formidable'),
    //AWS S3 settings*********************
    AWS = require('aws-sdk'),
    fs = require('fs'),
    path = require('path');
AWS.config.update({
    accessKeyId: '',
    secretAccessKey: ''
});

// ----------------------
@Injectable()
export class PictureGuideService {
    constructor(private commonQueryService: CommonQueryService,
        @InjectConnection('KNOW-MX_DB') private defaultConnection: Connection,
        private fileService: FileService
    ) { }

    async addPictureGuide(request: Request, body: object): Promise<any> {
        const pictureGuideModel = await getModel(request, PictureGuideSchema, PictureGuideModelName, this.defaultConnection);
        const result = await this.commonQueryService.addNewRecord(pictureGuideModel, body);
        return result;
    }

    async uploadFile(req: Request) {
        let images: string[] = [];
        const fileData = await this.getFileData(req);
        for (let i in fileData) {
            images.push(fileData[i]);
        }
        const result = await Promise.all(images.map(async (file) => {
            return await this.uploadToS3(file['_writeStream'].path);
        }));
        return result;
    }

    async getFileData(request: Request): Promise<any> {
        let form = new formidable.IncomingForm();
        form.uploadDir = './src/assets/images/';
        form.keepExtensions = true;
        form.maxFieldsSize = 10 * 1024 * 1024;
        form.multiples = true;
        return new Promise((resolve, reject) => {
            form.parse(request, function (err, fields, files) {
                if (err) {
                    throw err;
                    reject(err);
                }
                else {
                    resolve(files)
                }
            });
        });
    }

    async uploadToS3(fileStreamPath: string): Promise<string> {
        let s3 = new AWS.S3();
        let awsFolder = "all-files";
        // let filePath = fileData.filePath;
        // let bucketName = fileData.bucketName ? fileData.bucketName : defaultAwsBucketName;
        //configuring parameters
        let params = {
            Bucket: 'test-assests-me',
            Body: fs.createReadStream(fileStreamPath),
            Key: `${awsFolder}/` + Date.now() + "_" + path.basename(fileStreamPath)
        };

        return new Promise((resolve, reject) => {
            s3.upload(params, function (err, data) {
                if (err) {
                    reject(err);
                }
                if (data) {
                    resolve(data.Location);
                }
            });
        });
    }
}
