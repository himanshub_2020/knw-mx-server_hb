export interface IPictureGuide {
    Title: string;
    Keywords: string[];
    Steps: string;
    EmbedUrl: string;
    VideoUrl: string;
    CategoryPath: string;
    StartDate: string;
    EndDate: string;
    Language: object;
    LanguageGroupId: string;
    Version: object;
    LinkAnotherModules: string;
    Channels: string;
    Departments: string;
    CategoryId: string;
    Media: string;
    Urls: string;
}