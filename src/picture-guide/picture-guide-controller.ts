import { Controller, Get, Res, Req, Body, Query, Post, Delete, Put, UseGuards } from '@nestjs/common';
import { PictureGuideService } from "./picture-guide-service";
import { ResponseService } from "../shared/response.service";
import { ApiProperty, ApiCreatedResponse, ApiBody, ApiQuery, ApiHeader } from '@nestjs/swagger';
// import { AddDepartmentDTO, EditDepartmentDTO, fetchDepartmentsDTO, AddDepartmentDTO1, fetchDepartmentModel, addDepartmentModel, idModel, headerModel } from "./department.model";
import { ErrLogsService } from "../shared/err.logs.service";
import { ValidatorService } from "../shared/validator.service";
const controller = "src/department/departmentController";
import { AuthGuard } from "../shared/guards/auth.guard";
import { AddDepartmentDTO } from "../department/department.model";
import { PictureGuide } from "../picture-guide/picture-guide-schema";


@Controller('pictureGuide')  //endpoints - localhost:4000/api/v1/department
@UseGuards(AuthGuard)
export class PictureGuideController {
    constructor(private readonly pictureGuideService: PictureGuideService,
        private responseService: ResponseService,
        private errLogsService: ErrLogsService,
        private validatorService: ValidatorService,
    ) { }


    @ApiCreatedResponse({ description: 'Add a new department' })
    @ApiBody({ type: PictureGuide })
    @ApiHeader({
        name: 'hostName',
        description: 'Host name for database switching',
        required: true
    })
    @Post('addPictureGuide') //endpoints - localhost:4000/api/v1/department/addDepartment
    async addPictureGuide(@Req() request, @Body() bodyData, @Res() response): Promise<void> {
        try {
            // this.validatorService.validateBody(bodyData, addDepartmentModel);
            const result = await this.pictureGuideService.addPictureGuide(request, bodyData);
            this.responseService.okRes(response, result, `Record added successfully!`, result.length);
        }
        catch (err) {
            //Common ERR SERVICE that can used by any controller
            this.errLogsService.storeLog(err, `${controller}/addDepartment`);
            this.responseService.errRes(response, err);
        }
    }

    @Post('uploadFile')
    async uploadFile(@Req() request, @Res() response): Promise<void> {
        try {
            const result = await this.pictureGuideService.uploadFile(request);
            this.responseService.createdRes(response, result, `Files added successfully!`, result.length);
        }
        catch (err) {
            //Common ERR SERVICE that can used by any controller
            this.errLogsService.storeLog(err, `${controller}/addDepartment`);
            this.responseService.errRes(response, err);
        }
    }
}
