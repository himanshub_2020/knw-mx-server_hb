import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DepartmentModule } from './department/department.module';
import { SharedModule } from './shared/shared.module';
import { MongooseModule } from '@nestjs/mongoose';
import { DesignationModule } from './designation/designation.module';
import { ConfigModule } from '@nestjs/config';
import { PictureGuideModule } from './picture-guide/picture-guide.module';
import configuration from './config/configuration';

@Module({
  imports: [DepartmentModule, 
    ConfigModule.forRoot({
      load: [configuration],
      isGlobal: true,
    }),
    MongooseModule.forRoot(process.env.MONGO_DB_URL, {
      connectionName: 'KNOW-MX_DB',
    }),
    DesignationModule,
    PictureGuideModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
