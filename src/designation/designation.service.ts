import { Injectable, Body } from '@nestjs/common';

@Injectable()
export class DesignationService {
    async addNewDesignation(body: any): Promise<any> {
        return `The designation '${body.name}' is added successfully!`;
    }
}
