import { Controller, Get, Res, Req, Body, Query, Post, Delete, Put, Param } from '@nestjs/common';
import { DesignationService } from "./designation.service";
import { ResponseService } from "../shared/response.service";
import { CommonQueryService } from "../shared/commonQuery.services"
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { ErrLogsService } from "../shared/err.logs.service"
const controller = "src/desigination/desiginationController";
import { designationSchema, designationModelName } from "./designation.schema";
const { getModel } = require("../shared/dynamicDb");

@Controller('designation')  //endpoints - localhost:4000/api/v1/designation
export class DesignationController {
    constructor(private readonly designationService: DesignationService,
        private responseService: ResponseService,
        private commonQueryService: CommonQueryService,
        private errLogsService: ErrLogsService,
        @InjectModel("Designation") private designationModel: Model<any>
    ) { }

    @Post('addDesignation') //endpoints - localhost:4000/api/v1/designation/addDesignation
    async addDesignation(@Req() request, @Body() body, @Res() response): Promise<any> {
        try {
            console.log("body++++++ designation", body);
            const designationModel = await getModel(request, designationSchema, designationModelName);
            let condition = { name: body.name };
            if (await this.commonQueryService.checkIfAlreadyExists(designationModel, condition)) {
                return this.responseService.errRes(response, `Record already exists!`);
            } else {
                const result = await this.commonQueryService.addNewRecord(designationModel, body);
                //Common RESPONSE SERVICE that can used by any controller
                return this.responseService.successRes(response, [result], `Record added successfully!`, 0);
            }
        }
        catch (err) {
            //Common ERR SERVICE that can used by any controller
            this.errLogsService.storeLog(err, `${controller}/addDesignation`);
            return this.responseService.errRes(response, err);
        }
    }

    @Delete('deleteDesignation')
    async deleteDepartment(@Req() request, @Param() param, @Query() query, @Res() response): Promise<any> {
        const { id } = query || param,
            condition = { _id: id },
            dataToUpdate = { status: 'delete' };
        try {
            if (!id) {
                throw "Designation id is not found!";
            }
            const designationModel = await getModel(request, designationSchema, designationModelName);
            const result = await this.commonQueryService.editSingleRecord(designationModel, condition, dataToUpdate);
            return this.responseService.successRes(response, [result], `Record deleted successfully!`, 0);
        }
        catch (err) {
            //Common ERR SERVICE that can used by any controller
            this.errLogsService.storeLog(err, `${controller}/deleteDesignation`);
            return this.responseService.errRes(response, err);
        }
    }

    @Put('editDesignation/:id')
    async editDepartment(@Req() request, @Param() param, @Body() body, @Query() query, @Res() response): Promise<any> {
        const { id } = query || param,
            { name, description } = body,
            condition = { _id: id },
            dataToUpdate = { name, description };
        try {
            if (!id) {
                throw "Designation id is not found!";
            }
            console.log("-----------------" , condition , "^^^^^^^^^^" , dataToUpdate)
            const designationModel = await getModel(request, designationSchema, designationModelName);
            const result = await this.commonQueryService.editSingleRecord(designationModel, condition, dataToUpdate);
            return this.responseService.successRes(response, [result], `Record updated successfully!`, 0);
        }
        catch (err) {
            //Common ERR SERVICE that can used by any controller
            this.errLogsService.storeLog(err, `${controller}/editDesignation`);
            return this.responseService.errRes(response, err);
        }
    }

    // @Post('fetchDepartment')
    // async fetchDepartment(@Req() request, @Body() body, @Res() response): Promise<any> {
    //     const { isSingle, departmentId } = body;
    //     let result = "";
    //     if (isSingle) {
    //         result = await this.departmentService.fetchSingleDepartment(departmentId);
    //     } else {
    //         result = await this.departmentService.fetchDepartments();
    //     }
    //     return response.status(200).send({ code: 200, msg: result });
    // }
}
