import { Module } from '@nestjs/common';
import { SharedModule } from "../shared/shared.module";

import { DesignationService}  from "./designation.service";
import { DesignationController} from "./designation.controller";
import { MongooseModule } from '@nestjs/mongoose';
import { designationSchema } from "./designation.schema";

@Module({
imports : [
    // MongooseModule.forFeature([{ name: 'Designation', schema: designationSchema }]),

    MongooseModule.forFeature([{ name: 'Designation', schema: designationSchema }], 'KNOW-MX_DB'),

    SharedModule
],
providers:[DesignationService],
controllers:[DesignationController],
exports:[]

})
export class DesignationModule {}
