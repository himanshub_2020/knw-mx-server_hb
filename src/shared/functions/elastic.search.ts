
//---------------------------------------------------
const { Client } = require('@elastic/elasticsearch');
// const client = new Client({
//     cloud: {
//         id: 'i-o-optimized-deployment:ZWFzdHVzMi5henVyZS5lbGFzdGljLWNsb3VkLmNvbTo5MjQzJDE2ZWJmOGUxNzExOTRmYTZhM2FlMThlOTI2NGU3Mzc3JDBkMzlkZTE3MDEzZjRiN2Y5M2FkOWI0MzUwNzA0M2Rj',
//     },
//     auth: {
//         username: 'elastic',
//         password: 'HCxte65OOUanE4cEPj4rSCtY'
//     }
// })
console.log("##############>>>>>>>>>")
// const client = new Client({ node: 'http://localhost:9200' });
const client = new Client({ node: 'http://localhost:9200' });
//---------------------------------------------------

export const createElasticData = async (request: object, type: string, dataToSave: object): Promise<boolean> => {
    const hostName = request['headers'].hostName || request['headers'].hostname ? request['headers'].hostName || request['headers'].hostname : "";
    let indexResult = await client.index({
        index: hostName,
        // type: type,//this option is not supported from v6 of elastic 
        body: dataToSave
    });
    return true;
}

export const getElasticData = async (request: object, condition: object) => {

    const hostName = request['headers'].hostName || request['headers'].hostname ? request['headers'].hostName || request['headers'].hostname : "";
    await client.indices.refresh({ index: hostName });
    const { body } = await client.search({
        index: hostName,
        body: {
            query: {
                match: condition
            }
        }
    });
    console.log("elastic data>>>>>>>>>>>>", body.hits.hits);
}