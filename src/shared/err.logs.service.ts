import { Injectable, Body } from '@nestjs/common';
const logger = require('logger').createLogger('development.log')


@Injectable()
export class ErrLogsService {
    async storeLog(err, path): Promise<any> {
        logger.info(`${path} ==> ${err}`);
        return true;
    }
}
