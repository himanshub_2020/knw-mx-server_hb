import { Injectable } from '@nestjs/common';
import { Request } from 'express';
const formidable = require('formidable'),
    //AWS S3 settings*********************
    AWS = require('aws-sdk'),
    fs = require('fs'),
    path = require('path');
AWS.config.update({
    accessKeyId: '',
    secretAccessKey: ''
});

@Injectable()
export class FileService {
    async getFileData(request: Request): Promise<any> {
        let form = new formidable.IncomingForm();
        form.uploadDir = './src/assets/images/';
        form.keepExtensions = true;
        form.maxFieldsSize = 10 * 1024 * 1024;
        form.multiples = true;
        return new Promise((resolve, reject) => {
            form.parse(request, function (err, fields, files) {
                if (err) {
                    throw err;
                    reject(err);
                }
                else {
                    resolve(files)
                }
            });
        });
    }

    async uploadToS3(fileStreamPath: string): Promise<string> {
        let s3 = new AWS.S3();
        let awsFolder = "all-files";
        // let filePath = fileData.filePath;
        // let bucketName = fileData.bucketName ? fileData.bucketName : defaultAwsBucketName;
        //configuring parameters
        let params = {
            Bucket: 'test-assests-me',
            Body: fs.createReadStream(fileStreamPath),
            Key: `${awsFolder}/` + Date.now() + "_" + path.basename(fileStreamPath)
        };

        return new Promise((resolve, reject) => {
            s3.upload(params, function (err, data) {
                if (err) {
                    reject(err);
                }
                if (data) {
                    resolve(data.Location);
                }
            });
        });
    }
}
