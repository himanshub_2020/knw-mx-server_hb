import { Injectable, Body } from '@nestjs/common';
import { httpCodes } from "./constants";

@Injectable()
export class ResponseService {
    successRes(response: any, data: any, message: string, count: number): void {
        const code = httpCodes.ok;
        count = count ? count : 0;
        return response.status(code).send({ code, data, message, count });
    }

    errRes(response: any, err: any): void {
        const code = (err && err.code && err.code == 400) ? httpCodes.invalieReq : httpCodes.internalServerError,
            count = 0,
            data = [];
        err = (err && err.code && err.code == 400) ? err.err : err;
        return response.status(code).send({ code, data, err, count });
    }

    okRes(response: any, data: any, message: string, count: number): void {
        const code = httpCodes.ok;
        count = count ? count : 0;
        return response.status(code).send({ code, data, message, count });
    }

    createdRes(response: any, data: object, message: string, count: number): void {
        const code = httpCodes.created;
        count = count ? count : 0;
        return response.status(code).send({ code, data, message, count });
    }

    noContentRes(response: any): void {// specially for put/delete req where no need to send data in res body
        const code = httpCodes.noContent;
        return response.sendStatus(code);
    }
}
