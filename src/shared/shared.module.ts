import { Module, Global } from '@nestjs/common';
import { ResponseService } from "./response.service"
import { CommonQueryService } from "./commonQuery.services";
import { ErrLogsService } from "./err.logs.service";
import { ValidatorService } from "./validator.service";
import { FileService } from "./file-service";
@Global()
@Module({
    imports: [],
    providers: [ResponseService, CommonQueryService, ErrLogsService, ValidatorService, FileService],
    exports: [ResponseService, CommonQueryService, ErrLogsService, ValidatorService, FileService]
})
export class SharedModule { }
