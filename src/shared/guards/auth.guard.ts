import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { from, Observable } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate {
    canActivate(
        context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {
        const request = context.switchToHttp().getRequest();
        console.log("inside auth************************************************")
        return true;
    }
}