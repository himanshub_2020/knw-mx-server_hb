import { Injectable, Body } from '@nestjs/common';
import * as Validator from 'validatorjs';
import { httpCodes } from "./constants";

@Injectable()
export class ValidatorService {
    validateBody(body, rules): Boolean {
        let validation = new Validator(body, rules);
        if (validation.passes()) {
            return true;
        } else {
            //***********NOTE****** */
            //validation.errors.all(); returns obj and its keys are as the fields on which error found and value of these keys
            // as err msg that we need to find out that is why using below for-in loop to run a loop on abj
            // and concatinating all error messages into one variable (finalErrMsg)
            const errMsgObj = validation.errors.all();
            let finalErrMsg = "";
            for (const property in errMsgObj) {
                finalErrMsg = finalErrMsg + ", " + errMsgObj[property][0];
            }
            throw { code: httpCodes.invalieReq, err: finalErrMsg };
        }
    }
    validateParams(params, rules): boolean {
        let validation = new Validator(params, rules);
        if (validation.passes()) {
            return true;
        } else {
            //***********NOTE****** */
            //validation.errors.all(); returns obj and its keys are as the fields on which error found and value of these keys
            // as err msg that we need to find out that is why using below for-in loop to run a loop on abj
            // and concatinating all error messages into one variable (finalErrMsg)
            const errMsgObj = validation.errors.all();
            let finalErrMsg = "";
            for (const property in errMsgObj) {
                finalErrMsg = finalErrMsg + ", " + errMsgObj[property][0];
            }
            throw { code: httpCodes.invalieReq, err: finalErrMsg };
        }
    }
}
