export const httpCodes = {
    ok: 200,
    created: 201,
    noContent: 204,
    invalieReq: 400,
    unauthorized: 401,
    notFound: 404,
    internalServerError:500 
}